data "aws_ami" "ubuntu-latest" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_launch_configuration" "workers-lc" {
  name_prefix     = "workers-lc"
  image_id        = "${data.aws_ami.ubuntu-latest.id}"
  instance_type   = "${var.instance_type}"
  security_groups = ["${aws_security_group.on4u-testing-server.id}"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "workers-as" {
  name = "workers-as-${aws_launch_configuration.workers-lc.name
  }"

  launch_configuration = "${aws_launch_configuration.workers-lc.name}"
  min_size             = 1
  max_size             = 10
  vpc_zone_identifier  = ["${module.on4_vpc.private_subnets}"]
  health_check_type    = "EC2"
  default_cooldown     = 10

  tag {
    key                 = "Name"
    value               = "workers"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "workers-up" {
  name                   = "workers-up"
  scaling_adjustment     = 2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 30
  autoscaling_group_name = "${aws_autoscaling_group.workers-as.name}"
}

resource "aws_autoscaling_policy" "workers-down" {
  name                   = "workers-down"
  scaling_adjustment     = -2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 30
  autoscaling_group_name = "${aws_autoscaling_group.workers-as.name}"
}

resource "aws_cloudwatch_metric_alarm" "sqs-status-up" {
  alarm_name          = "sqs-status-up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"
  period              = "60"
  statistic           = "Sum"
  threshold           = "5"

  dimensions = {
    QueueName = "${aws_sqs_queue.on4u-queue.name}"
  }

  alarm_description = "This metric monitors sqs messages"
  alarm_actions     = ["${aws_autoscaling_policy.workers-up.arn}"]
}

resource "aws_cloudwatch_metric_alarm" "sqs-status-down" {
  alarm_name          = "sqs-status-down"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"
  period              = "60"
  statistic           = "Sum"
  threshold           = "5"

  dimensions = {
    QueueName = "${aws_sqs_queue.on4u-queue.name}"
  }

  alarm_description = "This metric monitors sqs messages"
  alarm_actions     = ["${aws_autoscaling_policy.workers-down.arn}"]
}
