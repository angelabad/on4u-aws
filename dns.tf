resource "aws_route53_zone" "on4u_com" {
  name = "on4u.com"
}

/*
resource "aws_route53_record" "test" {
  name    = "test"
  type    = "A"
  zone_id = "${aws_route53_zone.on4u_com.zone_id}"
  ttl     = "5"
  records = ["${aws_eip.on4u-testing-server-eip.public_ip}"]
}
*/
resource "aws_route53_record" "test" {
  zone_id = "${aws_route53_zone.on4u_com.zone_id}"
  name    = "test"
  type    = "A"

  alias {
    name                   = "${aws_lb.web.dns_name}"
    zone_id                = "${aws_lb.web.zone_id}"
    evaluate_target_health = true
  }
}
