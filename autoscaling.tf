resource "aws_launch_configuration" "web-lc" {
  name_prefix     = "web-lc"
  image_id        = "${data.aws_ami.on4u-2.id}"
  instance_type   = "${var.instance_type}"
  security_groups = ["${aws_security_group.on4u-testing-server.id}"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "web-as" {
  name = "web-as-${aws_launch_configuration.web-lc.name
  }"

  launch_configuration = "${aws_launch_configuration.web-lc.name}"
  min_size             = 2
  desired_capacity     = 2
  max_size             = 5
  vpc_zone_identifier  = ["${module.on4_vpc.private_subnets}"]
  health_check_type    = "ELB"

  target_group_arns = ["${aws_lb_target_group.web-servers.arn}"]

  tag {
    key                 = "Name"
    value               = "web"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
