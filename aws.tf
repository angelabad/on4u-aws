terraform {
  backend "s3" {
    bucket  = "terraform-on4u"
    key     = "production.tfstate"
    region  = "eu-west-1"
    profile = "angel"
  }
}

provider "aws" {
  region  = "eu-west-1"
  version = "~> 1.57"
  profile = "angel"
}

resource "aws_key_pair" "angel" {
  key_name   = "angel"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAuXOhC/K6IF+4THXWfjCaaB8Xjrwb7D0uc4BCU+p6SNszPkbAYwZHQvUxFzYNYd4InbYty0NP2If3HU3wAobWqNmucRvFrh8+HnIa8SIuN0uCaOKZ7Qw7NtT3gW1dsyXxzXRnLhQncYnXpm1iYeGANz2pxtaxmYZeYxq6c+AYS/oOFCVnK7//uBse4ozJe82wNU6mpnYyeBYPraH22FXH2OuZtEmJAih7AeWarFMQcJ3i6CdKrueNvH4hzt0tTUmNMCR0zq4BIn96aw2XYKUJb00i2mqNF6rtJM75LgtD0rotnSlVcUkVb4N2iRrvPbCFBzqJa5IPb9r5Whdc36qf1Q== angel@goa"
}
