resource "aws_sqs_queue" "on4u-queue" {
  name          = "on4u-out-queue"
  delay_seconds = 30

  //max_message_size          = 2048
  //message_retention_seconds = 86400
  //receive_wait_time_seconds = 10

  tags = {
    Environment = "production"
  }
}
