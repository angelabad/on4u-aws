resource "aws_lb" "web" {
  name               = "web-load-balancer"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.on4u-testing-server.id}"]
  subnets            = ["${module.on4_vpc.public_subnets}"]

  enable_deletion_protection = false

  // TODO: Meter logs en un bucket

  tags = {
    Environment = "testing"
  }
}

resource "aws_lb_target_group" "web-servers" {
  name     = "web-servers"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${module.on4_vpc.vpc_id}"
}

resource "aws_lb_listener" "web" {
  load_balancer_arn = "${aws_lb.web.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.web-servers.arn}"
  }
}
