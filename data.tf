data "aws_ami" "on4u-1" {
  most_recent = true
  name_regex  = "^version-1-"
  owners      = ["self"]
}

data "aws_ami" "on4u-2" {
  most_recent = true
  name_regex  = "^version-2-"
  owners      = ["self"]
}
