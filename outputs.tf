output "on4u-testing-lb-name" {
  value = "${aws_lb.web.dns_name}"
}

output "on4u-sqs-out-name" {
  value = "${aws_sqs_queue.on4u-queue.id}"
}
